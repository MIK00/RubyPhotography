﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RubyPhotography.WebMVC.Startup))]
namespace RubyPhotography.WebMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
